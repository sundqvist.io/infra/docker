#!/bin/sh

[ -z "$CI_PROJECT_NAME" ] && CI_PROJECT_NAME=${PWD##*/}
[ -z "$CI_REGISTRY_IMAGE" ] && CI_REGISTRY_IMAGE=$CI_PROJECT_NAME
[ -z "$CI_COMMIT_SHORT_SHA" ] && CI_COMMIT_SHORT_SHA="$(git rev-parse --short HEAD)"

PROJECT_NAME=${1:-$CI_PROJECT_NAME}
if [ $PROJECT_NAME = $CI_PROJECT_NAME ]; then
  IMAGE_NAME=$CI_REGISTRY_IMAGE
else
  IMAGE_NAME=$CI_REGISTRY_IMAGE/$PROJECT_NAME
fi
TAG=$CI_COMMIT_SHORT_SHA #$(date +%F)

shift
ARGS="$@"

echo $CI_PROJECT_NAME $CI_REGISTRY_IMAGE $CI_CI_COMMIT_SHORT_SHA

docker build --no-cache --tag $IMAGE_NAME:$TAG --tag $IMAGE_NAME:latest $ARGS . -f $PROJECT_NAME.Dockerfile && \
  docker push $IMAGE_NAME:$TAG && \
  docker push $IMAGE_NAME:latest
